import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../math/data2.dart';
import 'login.dart';

class WeldingPage6 extends StatefulWidget {
  final BevelType data;

  const WeldingPage6({Key? key, required this.data}) : super(key: key);

  @override
  State<WeldingPage6> createState() => _WeldingPage6State();
}

class _WeldingPage6State extends State<WeldingPage6> {
  final auth = FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
    print(widget.data.toJson().toString());
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference _weldData =
        FirebaseFirestore.instance.collection('weldData');
    Future<void> addData() {
      // Call the user's CollectionReference to add a new user
      return _weldData.add(widget.data.toJson());
    }

    return TextButton(
      onPressed: () {
        addData();
      },
      child: Text(
        "Add User",
      ),
    );

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          Center(
            child: Text(""),
          ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Text('Logout'),
                onPressed: () {
                  auth.signOut();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Back")),
                ],
              ),
            ],
          )
        ]),
      ),
    );
  }
}
