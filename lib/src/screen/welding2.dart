import 'dart:math';

// import 'package:nov/src/math/num_bevel.dart';
import 'package:flutter/material.dart';
import 'package:nov/src/screen/welding3.dart';
import 'package:nov/src/screen/welding1.dart';
import 'package:nov/themes.dart';
import 'package:vector_math/vector_math.dart' hide Colors;
import 'package:firebase_auth/firebase_auth.dart';

import '../math/data.dart';
import '../math/data.dart';
import '../math/data2.dart';
import 'login.dart';

class WeldingPage2 extends StatefulWidget {
  final BevelType data;

  const WeldingPage2({Key? key, required this.data}) : super(key: key);

  @override
  State<WeldingPage2> createState() => _WeldingPage2State();
}

class _WeldingPage2State extends State<WeldingPage2> {
  final auth = FirebaseAuth.instance;

  var thicknessController = TextEditingController();
  var t1Controller = TextEditingController();
  var t2Controller = TextEditingController();
  var bevelAngleController = TextEditingController();
  var radiansController = TextEditingController();
  var x1Controller = TextEditingController();
  var y1Controller = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print(widget.data.toJson().toString());
  }

  Widget buatKotak(Color warna, double ukuran, double ukuran1) {
    return Container(
        decoration: BoxDecoration(color: warna),
        height: ukuran,
        width: ukuran1,
        margin: EdgeInsets.all(10),
        alignment: Alignment(0.0, 0.0),
        child: Text(
          'STEP 3',
          style: TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.bold),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          Center(
            child: Text(""),
          ),
          SafeArea(
            child: Wrap(children: [buatKotak(Colors.black12, 50, 1000)]),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Text('Logout'),
                onPressed: () {
                  auth.signOut();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
              )
            ],
          ),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: thicknessController,
                  onChanged: (value) {
                    _t2();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Thickness Material',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: t1Controller,
                  onChanged: (value) {
                    _t2();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'T1',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: t2Controller,
                  onChanged: (value) {
                    _t2();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'T2',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: bevelAngleController,
                  onChanged: (value) {
                    _radians();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Bevel Angle',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: radiansController,
                  onChanged: (value) {
                    _radians();
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Radians',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Back")),
                ],
              ),
              Column(
                children: [
                  ElevatedButton(
                      onPressed: () {
                        widget.data.thickness =
                            double.parse(thicknessController.text.toString());
                        widget.data.t1 =
                            double.parse(t1Controller.text.toString());
                        widget.data.t2 =
                            double.parse(t2Controller.text.toString());
                        widget.data.bevelAngle =
                            double.parse(bevelAngleController.text.toString());
                        widget.data.rad =
                            double.parse(radiansController.text.toString());
                        widget.data.a2 =
                            double.parse(thicknessController.text.toString()) *
                                4;

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WeldingPage3(
                                      data: widget.data,
                                    )));
                      },
                      child: Text("Next")),
                ],
              ),
            ],
          )
        ]),
      ),
    );
  }

  void _t2() {
    if (thicknessController.text.trim().isNotEmpty &&
        t1Controller.text.trim().isNotEmpty) {
      final firstValue = double.parse(thicknessController.text);
      final secondValue = double.parse(t1Controller.text);

      t2Controller.text = (firstValue - secondValue).toString();
    }
  }

  void _radians() {
    if (bevelAngleController.text.trim().isNotEmpty) {
      if (widget.data.bevelType == "DB" || widget.data.bevelType == "SB") {
        final firstValue = double.parse(bevelAngleController.text);

        radiansController.text = (radians(firstValue)).toStringAsFixed(2);
      } else {
        final firstValue = double.parse(bevelAngleController.text);

        radiansController.text = (radians(firstValue) / 2).toStringAsFixed(2);
      }
    }
  }
}
