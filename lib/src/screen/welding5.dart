import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:nov/src/screen/welding6.dart';
import 'package:nov/themes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../math/data2.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import '../service/web_pdf.dart';

import 'login.dart';

class WeldingPage5 extends StatefulWidget {
  final BevelType data;

  const WeldingPage5({Key? key, required this.data}) : super(key: key);

  @override
  State<WeldingPage5> createState() => _WeldingPage5State();
}

class _WeldingPage5State extends State<WeldingPage5> {
  final auth = FirebaseAuth.instance;

  var totWeightController = TextEditingController();
  var lossFactorController = TextEditingController();
  var purchaseWeightController = TextEditingController();

  String? _valWeldingProccess;
  List _weldingProccess = [
    "SMAW",
    "GTAW",
    "GMAW",
    "SAW",
    "FCAW",
    "ESW",
  ];

  @override
  void initState() {
    super.initState();
    print(widget.data.toJson().toString());

    totWeightController.value =
        TextEditingValue(text: widget.data.totWeight.toString());
  }

  Widget buatKotak(Color warna, double ukuran, double ukuran1) {
    return Container(
        decoration: BoxDecoration(color: warna),
        height: ukuran,
        width: ukuran1,
        margin: EdgeInsets.all(10),
        alignment: Alignment(0.0, 0.0),
        child: Text(
          'STEP 6',
          style: TextStyle(
            color: Colors.black,
            fontSize: 15.0,
            fontWeight: FontWeight.bold,
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference _weldData =
        FirebaseFirestore.instance.collection('weldData');
    Future<void> addData() {
      // Call the user's CollectionReference to add a new user
      return _weldData
          .doc('doc_id')
          .set(widget.data.toJson())
          .then((value) => print("Data anda telah disimpan"))
          .catchError((error) => print("Failed to add user: $error"));
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(children: [
          Center(
            child: Text(""),
          ),
          SafeArea(
            child: Wrap(children: [buatKotak(Colors.black12, 50, 1000)]),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              FlatButton(
                child: Text('Logout'),
                onPressed: () {
                  auth.signOut();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => LoginScreen()));
                },
              )
            ],
          ),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: totWeightController,
                  onChanged: (value) {},
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Total Weight',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: DropdownButtonFormField(
                  isExpanded: true,
                  hint: Text("Welding Proccess"),
                  value: _valWeldingProccess,
                  items: _weldingProccess.map((value) {
                    return DropdownMenuItem(
                      child: Text(value),
                      value: value,
                    );
                  }).toList(),
                  onChanged: (value) {
                    if (value == "SMAW") {
                      lossFactorController.text = 1.5.toString();
                    } else if (value == "GTAW") {
                      lossFactorController.text = 1.1.toString();
                    } else if (value == "GMAW") {
                      lossFactorController.text = 1.05.toString();
                    } else if (value == "SAW") {
                      lossFactorController.text = 1.02.toString();
                    } else if (value == "FCAW") {
                      lossFactorController.text = 1.2.toString();
                    } else if (value == "ESW") {
                      lossFactorController.text = 1.1.toString();
                    }

                    widget.data.weldingProccess = value.toString();
                    purchaseWeightController.text =
                        (num.parse(lossFactorController.text) *
                                num.parse(widget.data.weight.toString()))
                            .toString();
                    setState(() {
                      _valWeldingProccess = value as String;
                    });
                  },
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: lossFactorController,
                  onChanged: (value) {},
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Loss Factor',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Form(
              child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: textWhiteGrey,
                    borderRadius: BorderRadius.circular(14)),
                margin: const EdgeInsets.all(10),
                child: TextFormField(
                  controller: purchaseWeightController,
                  onChanged: (value) {},
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: 'Purchase Weight',
                      hintStyle: heading6.copyWith(color: textGrey),
                      border: OutlineInputBorder(borderSide: BorderSide.none)),
                ),
              ),
            ],
          )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Back")),
                ],
              ),
              Column(
                children: [
                  ElevatedButton(
                      onPressed: () {
                        widget.data.totWeight =
                            num.parse(totWeightController.text.toString());
                        widget.data.lossFactor =
                            num.parse(lossFactorController.text.toString());
                        widget.data.puchaseWeight =
                            num.parse(purchaseWeightController.text.toString());
                        addData();
                        showAlertDialog(context);
                      },
                      child: Text("Finish")),
                ],
              ),
            ],
          )
        ]),
      ),
    );
  }

  Future<void> _cetakPDF() async {
    //Create a PDF document.
    final PdfDocument document = PdfDocument();
    document.pageSettings.orientation = PdfPageOrientation.landscape;
    //Add page to the PDF

    PdfGrid grid = PdfGrid();

    grid.columns.add(count: 30);
    grid.headers.add(1);

    PdfGridRow header = grid.rows.add();
    header.cells[0].value = 'No';
    header.cells[1].value = 'Nama';
    header.cells[2].value = 'kelas';

    PdfGridRow row = grid.headers[0];
    row.cells[0].value = 'No';
    row.cells[1].value = 'Nama';
    row.cells[2].value = 'kelas';

    grid.draw(
        page: document.pages.add(), bounds: const Rect.fromLTWH(0, 0, 0, 0));

    //Save the PDF document
    final List<int> bytes = document.save();
    //Dispose the document.
    document.dispose();
    //Save and launch the file.
    await saveAndLaunchFile(bytes, 'Invoice.pdf');
  }

  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget cetakPDF = FlatButton(
      child: Text("Cetak PDF"),
      onPressed: () {
        _cetakPDF();
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Data anda telah disimpan"),
      actions: [
        okButton,
        cetakPDF,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
